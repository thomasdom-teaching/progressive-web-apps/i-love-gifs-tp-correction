function setLoading(isLoading) {
    const loaderElement = document.getElementById('loader');
    const gifsElement = document.getElementById('gifs');
    if (isLoading) {
        loaderElement.style.display = null;
        gifsElement.style.display = 'none';
    }
    else {
        loaderElement.style.display = 'none';
        gifsElement.style.display = null;
    }
}

function addGIFToFavorite(event) {
    const likeButton = event.currentTarget;
    const gifId = likeButton.dataset.gifId;

    const gifElement = document.getElementById(gifId);

    const gifTitle = gifElement.querySelector('div h3').textContent;
    const gifVideoUrl = gifElement.querySelector('source').src;
    const gifImageUrl = gifElement.querySelector('img').src;

    const db = window.db;
    db.open()
        .catch(e => console.error('Could not open local db:', e));

    // Put GIF in local database
    db.transaction('rw', db.gifs, () => {
        db.gifs.add({
            id: gifId,
            title: gifTitle,
            imageUrl: gifImageUrl,
            videoUrl: gifVideoUrl,
        });
    }).catch(e => console.error(e));

    // Put GIF image in cache
    caches.open('gif-images')
        .then(cache => {
            cache.add(gifVideoUrl);
            cache.add(gifImageUrl);
        })
        .catch(e => console.error('Could not save GIF image to cache:', e));

    // Set button in 'liked' state (disable the button)
    likeButton.disabled = true;
}

function buildGIFCard(gifItem, isSaved) {
    // Create GIF Card element
    const newGifElement = document.createElement('article');
    newGifElement.classList.add('gif-card');
    newGifElement.id = gifItem.id;

    // Append GIF to card
    const gifImageElement = document.createElement('video');
    gifImageElement.autoplay = true;
    gifImageElement.loop = true;
    gifImageElement.muted = true;
    gifImageElement.setAttribute('playsinline', true);

    const videoSourceElement = document.createElement('source');
    videoSourceElement.src = gifItem.images.original.mp4;
    videoSourceElement.type = 'video/mp4';
    gifImageElement.appendChild(videoSourceElement);

    const imageSourceElement = document.createElement('img');
    imageSourceElement.classList.add('lazyload');
    imageSourceElement.dataset.src = gifItem.images.original.webp;
    imageSourceElement.alt = `${gifItem.title} image`;
    gifImageElement.appendChild(imageSourceElement);

    newGifElement.appendChild(gifImageElement);

    // Append metadata to card
    const gifMetaContainerElement = document.createElement('div');
    newGifElement.appendChild(gifMetaContainerElement);

    // Append title to card metadata
    const gifTitleElement = document.createElement('h3');
    const gifTitleNode = document.createTextNode(gifItem.title || 'No title');
    gifTitleElement.appendChild(gifTitleNode);
    gifMetaContainerElement.appendChild(gifTitleElement);

    // Append favorite button to card metadata
    const favButtonElement = document.createElement('button');
    favButtonElement.setAttribute('aria-label', `Save ${gifItem.title}`);
    favButtonElement.classList.add('button');
    favButtonElement.dataset.gifId = gifItem.id;
    favButtonElement.onclick = addGIFToFavorite;
    const favIconElement = document.createElement('i');
    favIconElement.classList.add('fas', 'fa-heart');
    favButtonElement.appendChild(favIconElement);
    gifMetaContainerElement.appendChild(favButtonElement);

    // Disable button (set GIF as liked) if liked
    if (isSaved) {
        favButtonElement.disabled = true;
    }

    // Append GIF Card to DOM
    const articlesContainerElement = document.getElementById('gifs');
    articlesContainerElement.appendChild(newGifElement);
}

window.addEventListener('DOMContentLoaded', async function () {
    setLoading(true);

    const url = new URL('gifs/trending', 'https://api.giphy.com/v1/');
    const queryParams = {
        api_key: 'WZLo855xWwYN1BrC5O5iNTBO3029pRHz',
        limit: 24,
        offset: 0,
    };
    url.search = new URLSearchParams(queryParams).toString();

    try {
        const response = await fetch(url);
        if (!response.ok) {
            // TODO: Set error screen
            return;
        }
        const giphyResponse = await response.json();

        const gifs = giphyResponse.data;

        const db = window.db;
        db.open().catch(e => console.error('Could not open database:', e));
        gifs.forEach(async gif => {
            const savedGifs = await db.gifs.get(gif.id);
            const isSaved = typeof (savedGifs) !== 'undefined';
            buildGIFCard(gif, isSaved);
        });
    } catch (e) {
        // TODO: Set error screen
    } finally {
        setLoading(false);
    }
});
